#!/usr/bin/perl
#=========================================================================
#
# Start window for COMPASS Frontend Database Administration
# Author: Lars Schmitt, February 2002
#
# From this window a single MODULE or list of MODULEs can be selected
#=========================================================================

require "fedb_lib.pl";

$query = new CGI;
print $query->header;
print $query->start_html(-title=>'Frontend Database',
                         -script=>{'src' => 'fedb.js'});

print $query->h1("Frontend Database");

# navigation bar
print "<p>";
print 'Navigation: <a href="fedb.pl">Top</a>';

if ($query->param('TABLE')) {
  my $table=$query->param('TABLE');
  my $tag=$query->param('TAG');
  print " / <a href=\"fedb.pl?TABLE=$table&TAG=$tag&START=Select\">$table.$tag</a>";
}

if ($query->param('SELECT')) {
  my $table  = $query->param('TABLE');
  my $tag    = $query->param('TAG');
  my $select = $query->param('SELECT');
  my $option = $query->param($select);
  print " / <a href=\"fedb.pl?TABLE=$table&TAG=$tag&$select=$option&SELECT=$select\">$select.$option</a>";
}

print "</p>\n";

# print $query->Dump();

# Select case for display function
if($query->param('START')) {
    &do_select($query);
}
elsif($query->param('EDIT')) {
    &do_edit($query);
}
elsif($query->param('Copy')) {
    &do_edit_again($query);
}
elsif($query->param('Submit')) {
    &do_submit($query);
}
elsif($query->param('SELECT')||$query->param('NEW')) {
    &do_show($query);
}
else {
    &do_start($query);
}


print $query->end_html;

#=========================================================================
# Subroutines
#=========================================================================

# Print start window
sub do_start {
    local($q)=@_;

    print
      $q->p("Select a table to view or modify"),
        $q->start_form(-method => 'GET'),
      '<table border="0">',
      $q->Tr([
        $q->td(["Table:", $q->popup_menu(-name=>"TABLE", -values=>\@Tables)]),
        $q->td(["Version tag:", $q->popup_menu(-name=>"TAG", -values=>\@Tags)]),
      ]),
      "</table>\n",
      "<p>",
      $q->submit(-name=>"START", -value=>"Select"),
      "</p>",
      $q->end_form;
}

# Print selection window
sub do_select {
    local($myquery) = @_;
    local($table)=$myquery->param("TABLE");
    local($tag)=$myquery->param("TAG");
    local(%DescHash)=&get_meta("STARTTABLE","$table","dropdown","description");
    local(%QueryHash)=&get_meta("STARTTABLE","$table","dropdown","query");

    print
      $myquery->p(
        "Choose table entries according to various selection criteria given below.",
        "Press the button on the right of the chosen criterion.");

    # Place the version tag
    print '<table border="0">'."\n";
    foreach my $dropdown (keys %QueryHash) {
        $QueryHash{$dropdown} =~ s/XXX/$tag/;
        # print "$dropdown: ", $QueryHash{$dropdown},"\n";

        # Print forms with dropdown menues 
        my @dropvalues=&get_array($QueryHash{$dropdown});
        unshift @dropvalues, '<--ALL-->';
        print
            $myquery->start_form(-method => 'GET'),
            $myquery->hidden(-name=>"TABLE",-value=>"$table"),
            $myquery->hidden(-name=>"TAG",-value=>"$tag"),
            '<tr><td>',
            $DescHash{$dropdown},':</td><td style="text-align:right">',
            $myquery->popup_menu(-name=>"$dropdown",
                               -values=>\@dropvalues),"</td><td>",
            $myquery->submit(-name=>"SELECT",
                           -value=>"$dropdown"),"</td></tr>\n",
            $myquery->end_form;
    }
    print "</table>\n";

    print
      $myquery->p(
        "You may also identify one row by giving its primary keys.",
        "If it does not exist, you may create a new entry to the table.");

    # Print a form to enter a new row
    print
        $myquery->start_form(-method => 'GET'),
        $myquery->hidden(-name=>"TABLE",-value=>"$table"),
        $myquery->hidden(-name=>"TAG",-value=>"$tag");
        #"Create a new entry in $table: ";
    # One textfield per primary key
    foreach my $newkey (split(/$;/,$Primary{$table})) {
        if("$newkey" ne "Version_tag") {
            print
                "$newkey :",$myquery->textfield(-name=>"$newkey",
                                                -size=>15);
        }
    }
    print
        $myquery->submit(-name=>"NEW",
                         -value=>"$table"),
        $myquery->end_form;
}

# Show selected entries
sub do_show {
    local($myquery)=@_;
    local($table)=$myquery->param("TABLE");
    local($tag)=$myquery->param("TAG");

    # local(%DescHash)=&get_meta("METATABLE","$table","colname","description");
    local(%TypeHash)=&get_meta("METATABLE","$table","colname","type");
    local(%DropHash)=&get_meta("METATABLE","$table","colname","dropdown");
    local(%PrioHash)=&get_meta("METATABLE","$table","colname","prio");
    local(@primary)=split(/$;/,$Primary{$table});

    my $primary=join(",",@primary);
    my @tablevalues = sort { $PrioHash{$a} <=> $PrioHash{$b} } keys %PrioHash;
    my $tablevalues = join(",",@tablevalues);
 
    local(%OrderHash)=&get_meta("METATABLE","$table","colname","orderby");
    my @ordervalues = sort { $OrderHash{$b} <=> $OrderHash{$a} } keys %OrderHash;
    my $orderby = join(",",@ordervalues);

    print
      $myquery->p(
        "This page shows the selected rows of the table. To continue editing ",
        "press the <em>EDIT</em> button below.");

    # SELECT or NEW - compose a query for all entries
    my $sql;
    if($myquery->param("SELECT")) {
        my $select=$myquery->param("SELECT");
        my $selected=$myquery->param("$select");
        my $cond;
        $cond="AND $select='$selected' " unless $selected eq '<--ALL-->';
        $sql="SELECT $tablevalues from $table where Version_tag='$tag' ".
          $cond."order by $orderby";
    } else {
        # A unique primary key is given
        my $string=sprintf("SELECT %s from %s where Version_tag='%s'",
                           $tablevalues,$table,$tag);
        foreach my $newkey (@primary) {
            if("$newkey" ne "Version_tag") {
                $sql=sprintf("%s AND %s like '%s'",$string,$newkey,
                                $myquery->param("$newkey"));
            }
        }
    }
    #print "SQL: $sql\n"; 
    my %tableinfo=&get_hash($table,$sql);
    # print "IDS:",join(" - ",keys %tableinfo),"\n<BR>";

    if(defined %tableinfo) {
        # Print query results
        &print_table($myquery,%tableinfo);
    } else {
        # If not found create a new entry
        # Note: Primary keys apart from Version_tag have to be first (Prio)
        my @idkey;
        foreach my $newkey (@primary) {
            if("$newkey" ne "Version_tag") {
                my $pkey=$myquery->param("$newkey");
                push(@idkey,$pkey);
            }
        }
        my $theid=join(":",@idkey);
        my $theinfo=join("$;",@idkey);
        $tableinfo{$theid}=$theinfo;
        my $newtag="NEW";
        &edit_table($myquery,$newtag,%tableinfo);
    }
}

# Edit query
sub do_edit {
    local($myquery) = @_;
    local(@tableids)=sort split(/$;/,$query->param("TABLEIDS"));
    local($table)=$myquery->param("TABLE");
    local($tag)=$myquery->param("TAG");
    local(%tableinfo);
    my @primary=split(/$;/,$Primary{$table});
    local(%PrioHash)=&get_meta("METATABLE","$table","colname","prio");

    my @tablevalues = sort { $PrioHash{$a} <=> $PrioHash{$b} } keys %PrioHash;
    my $tablevalues=join(",",@tablevalues);

    local(%OrderHash)=&get_meta("METATABLE","$table","colname","orderby");
    my @ordervalues = sort { $OrderHash{$b} <=> $OrderHash{$a} } keys %OrderHash;
    my $orderby = join(",",@ordervalues);

    &print_edit_help();

    foreach $tableid (@tableids) {
        my $string = "select $tablevalues from $table where ";
        my @keys = split(/:/,$tableid);

        $i=0;
        foreach $key (@primary) {
            if($key ne "Version_tag") {
                my $string2 = sprintf("%s %s='%s' AND ",$string,
                                      $key,$keys[$i++]);
                $string=$string2;
            }
        }
        $sql=sprintf("%s Version_tag='%s' ORDER by %s",$string,$tag,$orderby);
        # print "$sql\n";
        my %tmpinfo=&get_hash($table,$sql);
        foreach my $id (keys %tmpinfo) {
            $tableinfo{$id}=$tmpinfo{$id};
        }
    }
    my $newtag="EDIT";
    &edit_table($myquery,$newtag,%tableinfo);
}

# Edit query again (after COPY command)
sub do_edit_again {
    local($myquery) = @_;
    local(@tableids)=sort split(/$;/,$myquery->param("TABLEIDS"));
    local($table)=$myquery->param("TABLE");
    local(%tableinfo);
    my @primary=split(/$;/,$Primary{$table});
    local(%PrioHash)=&get_meta("METATABLE","$table","colname","prio");
    local(%TypeHash)=&get_meta("METATABLE","$table","colname","type");

    my @tablevalues = sort { $PrioHash{$a} <=> $PrioHash{$b} } keys %PrioHash;
    my $tablevalues=join(",",@tablevalues);

    &print_edit_help();

    # Build string of values for table
    foreach my $mytable (@tableids) {
        my $fmytable=&string_to_var($mytable);

        # Get table info from query
        my @tableinfo;
        foreach $column (@tablevalues) {
            push(@tableinfo,$myquery->param("$column$fmytable"));            
        }
        # Rows marked "COPY" will be duplicated. For multiple copies always the
        # last pasted entry must be copied, or else it is overwritten.
        if($myquery->param("EDITTAG$fmytable") eq "COPY") {
            $tableinfo{$mytable}=join($;,@tableinfo);
            $myquery->param("EDITTAG$fmytable","EDIT");
            $tableinfo{"n$mytable"}=join($;,@tableinfo);
            $myquery->param("EDITTAG$fmytable","EDIT");
            $myquery->add_parameter("EDITTAGn$fmytable");
            push (@{$query->{"EDITTAGn$fmytable"}},"NEW");
        # All other cases except "IGN" are kept. "IGN" rows will be skipped 
        # from furhter editing.
        } elsif($myquery->param("EDITTAG$fmytable") ne "IGN") {
            $tableinfo{$mytable}=join($;,@tableinfo);
        }
    }
    my $newtag="EDIT";
    &edit_table($myquery,$newtag,%tableinfo);
}

# Submit query
sub do_submit {
    local($myquery) = @_;
    local(@tableids)=sort split(/$;/,$myquery->param("TABLEIDS"));
    local($table)=$myquery->param("TABLE");
    local($tag)=$myquery->param("TAG");
    local(%tableinfo);
    my @primary=split(/$;/,$Primary{$table});
    local(%PrioHash)=&get_meta("METATABLE","$table","colname","prio");
    local(%TypeHash)=&get_meta("METATABLE","$table","colname","type");

    my @tablevalues = sort { $PrioHash{$a} <=> $PrioHash{$b} } keys %PrioHash;
    my $tablevalues=join(",",@tablevalues);


    # Build string of values for table
    foreach my $mytable (@tableids) {
        my $fmytable=&string_to_var($mytable);

        # Get table info from query
        my @tableinfo;
        foreach $column (@tablevalues) {
            push(@tableinfo,$myquery->param("$column$fmytable"));            
        }

        my @pkeys=split(/:/,$mytable);

        # Build a uniq search string for this entry
        my $j=0;
        my @where;
        foreach $pkey (@pkeys) {
            my $wstring;
            if(&is_number($pkey)) {
                $wstring=sprintf("%s=%s",$primary[$j],$pkey);
            } else {
                $wstring=sprintf("%s='%s'",$primary[$j],$pkey);
            }
            push(@where,$wstring);
            $j++;
        }
        my $where=join(" AND ",@where);

        # Update the table with backup
        if($myquery->param("EDITTAG$fmytable") eq "EDIT") {
            my $i=0;
            my @update;
            foreach my $column (@tablevalues) {
                if("$column" ne "Modification_date") {
                    my $type=$TypeHash{$column};
                    my $infoitem=$tableinfo[$i];
                    if("$infoitem" eq "") {
                        $infoitem="NULL";
                    }
                    if(("$type" eq "x") || ("$type" eq "i")) {
                        $infoitem=hex($infoitem) if $infoitem=~/^0x/;
                    }

                    my $string;
                    if((&is_number($infoitem)) || ("$infoitem" eq "NULL")) {
                        $string=sprintf("%s=%s",$column,$infoitem);
                    } else {
                        $string=sprintf("%s='%s'",$column,$infoitem);
                    }
                    push(@update,$string);
                }
                $i++;
            }
            $update=join(",",@update);

# print "<b>update_table:</b> where $where, update $update, query $myquery<br>\n";
# print $myquery->Dump();
            &update_table($where,$update,$myquery);
        # Delete row from table
        } elsif($myquery->param("EDITTAG$fmytable") eq "DEL") {
            &delete_table($where,$myquery);
        # Store new table
        } elsif($myquery->param("EDITTAG$fmytable") eq "NEW") {
            my $i=0;
            my @values;
            my @columns;
            foreach my $column (@tablevalues) {
                if("$column" ne "Modification_date") {
                    my $type=$TypeHash{$column};
                    my $infoitem=$tableinfo[$i];
                    my $vstring="";
                    if(("$type" eq "x") || ("$type" eq "i")) {
                        $infoitem=hex($infoitem) if $infoitem=~/^0x/;
                    }

                    if((&is_number($infoitem)) || ("$infoitem" eq "NULL")){
                        $vstring=sprintf("%s",$infoitem);
                    } else {
                        $vstring=sprintf("'%s'",$infoitem);
                    }
                    push(@values,$vstring);
                    push(@columns,$column);
                }
                $i++;
            }
            $columns=join(",",@columns);
            $values=join(",",@values);

            &store_table($columns,$values,$myquery);
        }
    }
    &do_show($myquery);
}

# Print table entries
sub print_table {
    local($myquery,%tableinfo)=@_;
    local($table)=$myquery->param("TABLE");
    local($tag)=$myquery->param("TAG");
    local(@tableids)=sort keys %tableinfo;
    @tableids = sort {$a <=> $b} @tableids if $tableids[$#tableids] lt 'a';
    local(%TypeHash)=&get_meta("METATABLE","$table","colname","type");
    local(%PrioHash)=&get_meta("METATABLE","$table","colname","prio");
    local(%DropHash)=&get_meta("METATABLE","$table","colname","dropdown");
    local(%LinkHash)=&get_meta("METATABLE","$table","colname","link");
    my @tablevalues = sort { $PrioHash{$a} <=> $PrioHash{$b} } keys %PrioHash;

    print
        $myquery->start_multipart_form(-action=>$myquery->script_name),
        "<TABLE BORDER=1 CELLPADDING=4 CELLSPACING=0>",
        "<THEAD><TR VALIGN=TOP>\n";

    # Table head
    foreach my $elem (@tablevalues) {
        my $e=$elem;
        $e =~ s/_/ /g;
        print "<TH>$e</TH>\n";
    }
    print "</TR></THEAD><TBODY>\n";

    # Table contents
    foreach my $id (@tableids) {
        my @info=split(/$;/,$tableinfo{$id});
        print "<TR VALIGN=TOP>\n";

        my $i=0;
        foreach my $infoitem (@info) {
            my $type=$TypeHash{$tablevalues[$i]};
            if($type eq "x") {
              $infoitem=sprintf("0x%x",$infoitem);
            } elsif($type eq "d") {
              $infoitem =~ s/ /&nbsp;/;
            }
            my $drop=$DropHash{$tablevalues[$i]};
            if (defined($drop) && substr($drop,0,4) eq 'list') {
              my $split=substr($drop,4,1);
              for my $d (split(/$split/,substr($drop,5))) {
                my ($a,$b)=split(/:/,$d,2);
                if($infoitem eq $a) {
                  $infoitem=$b if defined($b);
                  last;
                }
              }
            }
            my $link=$LinkHash{$tablevalues[$i]};
            if($link) {
              my %desc;
              for my $d (split(/,/,$link)) {
                my @a=split(/=/,$d,2);
                $desc{$a[0]}=$a[1];
              }
              my $item='';
              for my $c (unpack('C*',$infoitem)) {
                $item.=sprintf('%%%02x',$c);
              }
              $infoitem="<a href=\"fedb.pl?TABLE=$desc{'table'}&TAG=$tag&".
                "SELECT=$desc{'select'}&$desc{'select'}=$item\">$infoitem</a>";
            } else {
              # append hyperlink to the macros name definition
              while($infoitem =~ /\$\{([^}]+)\}/g) {
                my $oldlen=length($1);
                pos($infoitem)--;
                my $p=pos($infoitem);
                my $before=substr($infoitem,0,$p-$oldlen);
                my $after=substr($infoitem,$p);
                my $l='';
                for my $c (unpack('C*',$1)) {
                  $l.=sprintf('%%%02x',$c);
                }
                my $middle="<a href=\"fedb.pl?TABLE=MACROS&TAG=$tag&".
                  "SELECT=Name&Name=$l\">$1</a>";
                $infoitem=$before.$middle.$after;
                pos($infoitem)=$p+length($middle)-$oldlen;
              }
            }
            print "<TD>$infoitem</TD>\n";
            $i++;
        }

        print "</TR>\n";
    }
    
    print "</TBODY></TABLE>";

    # Hidden information as defaults for editing
    my $tableids=join("$;",@tableids);
    my $select=$myquery->param('SELECT');
    my $selected=$myquery->param($select);
    print
        $myquery->hidden(-name=>"TABLEIDS",-value=>$tableids),
        $myquery->hidden(-name=>"TABLE",-value=>$table),
        $myquery->hidden(-name=>"TAG",-value=>$tag),
        $myquery->hidden(-name=>"SELECT",-value=>$select),
        $myquery->hidden(-name=>$select,-value=>$selected),
        "<p>",
        $myquery->submit(-name=>"EDIT",-value=>"Edit"),
        "</p>\n",
        $myquery->end_form;
}

# Edit table entries
sub edit_table {
    local($myquery,$newtag,%tableinfo)=@_;
    local($table)=$myquery->param("TABLE");
    local($tag)=$myquery->param("TAG");
    local(@ids)=sort keys %tableinfo;
    @ids = sort {$a <=> $b} @ids if $ids[$#ids] lt 'a';
    local(%TypeHash)=&get_meta("METATABLE","$table","colname","type");
    local(%PrioHash)=&get_meta("METATABLE","$table","colname","prio");
    local(%DropHash)=&get_meta("METATABLE","$table","colname","dropdown");
    my @tablevalues = sort { $PrioHash{$a} <=> $PrioHash{$b} } keys %PrioHash;
    my @primary=split(/$;/,$Primary{$table});

    # Mark a new entry
    # if("$newtag" ne "NEW") {
    #        push(@ids,"ALL");
    #   $tableinfo{"ALL"}="ALL";
    #}

    # print "edit_tables ids: ",join("/",@ids),"<BR>\n";

    print
        $myquery->start_multipart_form(-action=>$myquery->script_name),
        "<TABLE BORDER=1 CELLPADDING=4 CELLSPACING=0>",
        "<THEAD><TR VALIGN=TOP>\n",
        "<TH>&nbsp;</TH>\n",
        $myquery->th(\@tablevalues),
        "</TR></THEAD><TBODY>\n";

    # Print table for editing 
    foreach my $mytable (@ids) {
        my $fmytable=&string_to_var($mytable);

        my @tableinfo=split(/$;/,$tableinfo{$mytable});

        # The edittag handles new, copied etc. lines. Default is EDIT or NEW
        my $edittag="$newtag";
        if($myquery->param("EDITTAG$fmytable")) {
            $edittag=$myquery->param("EDITTAG$fmytable");
        }
        print 
            "<TR VALIGN=TOP>\n",
            "<TD>",$myquery->popup_menu(-name=>"EDITTAG$fmytable",
                                        -values=>\@edactions,
                                        -default=>"$edittag"),
            "</TD>\n";

        my $i=0;
        foreach my $column (@tablevalues) {
            my $infoitem = $tableinfo[$i];
            if(!defined($infoitem)) {
                $infoitem='';
            }
            my $type=$TypeHash{$column};
            my $drop=$DropHash{$column};
            # if((substr($drop,0,5) eq "print") || (grep(/$column/,@primary))){
            #if("$infoitem" eq "") {
            #$infoitem="NULL";
            #}

            if(substr($drop,0,5) eq "print"){
                if($type eq "d") {
                  $infoitem=~s/ /&nbsp;/;
                }
                # Only show the information
                print 
                    "<TD>$infoitem",
                    $myquery->hidden(-name=>"$column$fmytable",
                                     -value=>$infoitem),
                    "</TD>\n";
            } elsif("$drop" eq ""){
                if(("$type" eq "x") || ("$type" eq "i")) {
                    $infoitem=hex($infoitem) if $infoitem=~/^0x/;
                }
                # Hex format
                if($type eq "x") {
                  $infoitem=sprintf("0x%x",$infoitem);
                }
                # Create a textfield
                print 
                    "<TD>",$myquery->textfield(-name=>"$column$fmytable",
                                               -size=>length("$column"),
                                               -default=>$infoitem),
                    "</TD>\n";
            } elsif(substr($drop,0,4) eq "list") {
                # Create a popup with predefined values
                my $values = substr($drop,5);
                my $split = substr($drop,4,1);
                my @values;
                my %labels;
                for my $s (split(/$split/,$values)) {
                  my ($a,$b) = split(/:/,$s,2);
                  $b = $a unless defined($b);
                  push @values, $a;
                  $labels{$a} = $b;
                }

                print 
                    "<TD>",$myquery->popup_menu(-name=>"$column$fmytable",
                                                -values=>\@values,
                                                -default=>$infoitem,
                                                -labels=>\%labels),
                    "</TD>\n";
            } else {

                # Place the version tag 
                $drop =~ s/XXX/$tag/;
                # print "Query: ",$drop,"<BR>\n";
                # Create a popup menu with values from the DB
                my @values = &get_array($drop);

                print 
                    "<TD>",$myquery->popup_menu(-name=>"$column$fmytable",
                                                -values=>\@values,
                                                -default=>$infoitem),
                    "</TD>\n";
            }
            $i++;
        }

        print "</TR>\n";
    } 

    print "<TR><TD>ALL</TD>";
    my $ALL="ALL";
    foreach my $column (@tablevalues) {
        my $type=$TypeHash{$column};
        my $drop=$DropHash{$column};

        if(substr($drop,0,5) eq "print"){
            # Empty field
            print "<TD>&nbsp;</TD>\n";
        } elsif("$drop" eq ""){
            # Create a textfield
            print 
                "<TD>",$myquery->textfield(-name=>"$column$ALL",
                                           -size=>length("$column"),
                                           -onChange=>"copyAll(this)",
                                           -onInput=>"copyAll(this)"),
                "</TD>\n";
        } elsif(substr($drop,0,4) eq "list") {
            # Create a popup with predefined values
            my $values = substr($drop,5);
            my $split = substr($drop,4,1);
            my @values;
            my %labels;
            for my $s (split(/$split/,$values)) {
              my ($a,$b) = split(/:/,$s,2);
              $b = $a unless defined($b);
              push @values, $a;
              $labels{$a} = $b;
            }

            print 
                "<TD>",$myquery->popup_menu(-name=>"$column$ALL",
                                            -values=>\@values,
                                            -onChange=>"copyAll(this)",
                                            -labels=>\%labels),
                "</TD>\n";
        } else {

            # Place the version tag 
            $drop =~ s/XXX/$tag/;
            # print "Query: ",$drop,"<BR>\n";
            # Create a popup menu with values from the DB
            my @values = &get_array($drop);

            print 
                "<TD>",$myquery->popup_menu(-name=>"$column$ALL",
                                            -values=>\@values,
                                            -onChange=>"copyAll(this)"),
                "</TD>\n";
        }
    }
    print "</TR>\n";

    print "</TBODY></TABLE>";

    my $tableids=join("$;",@ids);

    # Pass over information as hidden values
    $myquery->param("TABLEIDS","$tableids");
    my $select=$myquery->param('SELECT');
    my $selected=$myquery->param($select);
    print
        $myquery->hidden(-name=>"TABLEIDS",-value=>$tableids),
        $myquery->hidden(-name=>"TABLE",-value=>$table),
        $myquery->hidden(-name=>"TAG",-value=>$tag),
        $myquery->hidden(-name=>"SUBMITTAG",-value=>$newtag),
        $myquery->hidden(-name=>"SELECT",-value=>$select),
        $myquery->hidden(-name=>$select,-value=>$selected),
        "<p>",
        $myquery->reset(),"&nbsp;&nbsp;&nbsp;\n",
        $myquery->submit(-name=>"Copy",
                       -value=>"Copy"),"&nbsp;&nbsp;&nbsp;\n",
        $myquery->submit(-name=>"Submit",
                       -value=>"Submit"),
        "</p>\n",
        $myquery->end_form;
}

# Some printout as help comments
sub print_edit_help {
    print 
        "You may edit all or some entries. The edit tags in front mean:<BR>\n",
        "<UL><LI> <em>EDIT</em>: by default the row is editable</LI>",
        "<LI><em>IGN</em> will ignore the row and omit it after redisplay </LI>",
        "<LI><em>COPY</em> will mark rows to be duplicated as template for new rows</LI>",
        "<LI><em>DEL</em> marks the row for deletion. Only a backup copy is kept.</LI>",
        "<LI><em>NEW</em> will be set for new or copied rows. You have to set this manually ", 
        "if you do multiple copies of the same row always copying the last copy</LI></UL>\n",
        "The last row is labelled <em>ALL</em>. Changing any value here affects the whole ",
        "column, except where <em>IGN</em> is set.<BR>\n",
        "<P>\n",
        "Below are three buttons:<BR>\n",
        "<UL><LI> <em>Reset</em> restores previous data. It does not reverse submitted ",
        "<em>COPY</em> or <em>IGN</em> requests.</LI>\n",
        "<LI> <em>Copy</em>: enacts <em>COPY</em> and <em>IGN</em> selections ",
        "updating this editing window accordingly.</LI>\n",
        "<LI> <em>Submit</em>: submits the changes into the database.</LI></UL>\n";
}

# transform string into valid script variable name
sub string_to_var {
  my ($s) = @_;
  $s =~ s/_/__/g;
  while($s =~ /([^a-zA-Z0-9_])/g) {
    my $p = pos($s);
    $s = substr($s,0,$p-1).sprintf('_%02X',unpack('C',$1)).substr($s,$p);
    pos($s) = $p+2;
  }
  return $s;
}
