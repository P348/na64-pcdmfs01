#!/usr/bin/perl
#=========================================================================
#
# Common library routines for COMPASS Frontend Database Administration
# Author: Lars Schmitt, February 2002
#
# Updating this file should be sufficient to keep the structure
#=========================================================================

# Edit actions
@edactions = ("EDIT", "IGN", "COPY", "DEL", "NEW");

# The packages
use CGI;
use DBI;
use POSIX qw(strftime);
$The_date = strftime "%Y%m%d%H%M%S", localtime;

# Connect to our database
$dbh = DBI->connect("DBI:mysql:devdb:localhost", "daq", "daq");
# need to add our own error handling
$dbh->{HandleError} = sub {print '<font color=red><h1>An Error Occured:</h1><p>'.$_[0].'</p></font>';die $_[0];};

#DBI->trace(2);
#=========================================================================
# Definitions of hierarchy and DB components from METATABLE and STARTTABLE
#=========================================================================
@Tables=&get_column("METATABLE","tablename");
@Tags=&get_column("VERSION_TAGS","name");
@MetaNames=&get_desc("METATABLE");
# print "Tags: ",join(", ",@Tags),"\n";
foreach $table (@Tables) {
        $Primary{$table}=join("$;",&get_primary("$table"));
        #print "$table: ",$Primary{$table},"\n";
}

# %ans=&get_meta("STARTTABLE","FRONTEND","dropdown","query");
# print join(", ",keys %ans),"\n";
# print join(", ",values %ans),"\n";

# %qans=&get_meta("STARTTABLE","MODULE","dropdown","query");
# print join(", ",keys %qans),"\n";
# print join(", ",values %qans),"\n";

#=========================================================================
# Basic subroutines
#=========================================================================

# Get all entries for a column in a table - returns an array
sub get_col_tag {
    local($tablename,$colname) = @_;
    local(@cols);

    # The statement handle
    my $string=sprintf("SELECT %s FROM %s",$colname,$tablename);
    my $prepare_string=sprintf("%s WHERE Version_tag='latest'",$string);
    my $sth = $dbh->prepare("$prepare_string");
      

    # Execute the statement
    $sth->execute();
    my $array_ref = $sth->fetchall_arrayref();

    # Put each different col entry in an array
    foreach my $col (@$array_ref) {
        my $pattern=$$col[0];
        if(locate($pattern, @cols)==0) {
            push(@cols,$$col[0]);
        }
    }
    return @cols;
}

# Get all entries for some columns in a table - returns a hash
sub get_cols_tag {
    local($tablename,$colnames) = @_;
    local(%colinfo);
    local(%ids);

    # The statement handle
    my $string=sprintf("SELECT %s FROM %s",$colnames,$tablename);
    my $prepare_string=sprintf("%s WHERE Version_tag='latest'",$string);
    # print "$prepare_string\n";
    my $sth = $dbh->prepare("$prepare_string");

    # Execute the statement
    $sth->execute();
    my $array_ref = $sth->fetchall_arrayref();

    # Put each result in the array of table instances
    foreach my $col (@$array_ref) {
        my $pattern=$$col[0];
        if(locate($pattern, @ids)==0) {
            my $id = $$col[0];
            push(@ids,$id);
            $colinfo{$id}=join("$;",@$col);
        }
    }
    return %colinfo;
}

# Get all entries for a col in a table - returns an array
sub get_column {
    local($tablename,$colname) = @_;
    local(@cols);

    # The statement handle
    my $prepare_string=sprintf("SELECT distinct %s FROM %s",
                               $colname,$tablename);
    my $sth = $dbh->prepare("$prepare_string");

    # Execute the statement
    $sth->execute();
    my $array_ref = $sth->fetchall_arrayref();

    # Put each different col entry in an array
    foreach my $col (@$array_ref) {
        my $pattern=$$col[0];
        if(locate($pattern, @cols)==0) {
            push(@cols,$$col[0]);
        }
    }
    return @cols;
}

# Get all entries for some columns in a table - returns a hash
sub get_columns {
    local($tablename,$colnames) = @_;
    local(%colinfo);
    local(%ids);

    # The statement handle
    my $prepare_string=sprintf("SELECT %s FROM %s",$colnames,$tablename);
    # print "$prepare_string\n";
    my $sth = $dbh->prepare("$prepare_string");

    # Execute the statement
    $sth->execute();
    my $array_ref = $sth->fetchall_arrayref();

    # Put each result in the array of table instances
    foreach my $col (@$array_ref) {
        my $pattern=$$col[0];
        if(locate($pattern, @ids)==0) {
            my $id = $$col[0];
            push(@ids,$id);
            $colinfo{$id}=join("$;",@$col);
        }
    }
    return %colinfo;
}

# Get a hash of items for a table
sub get_meta {
    local($table,$tablename,$keyname,$valname) = @_;
    local(%metahash);
    local(@ids);

    # The statement handle
    my $string="select $keyname,$valname from $table where tablename=\'$tablename\'";
    # print "$string\n";
    my $sth = $dbh->prepare("$string");

    # Execute the statement
    $sth->execute();
    my $array_ref = $sth->fetchall_arrayref();

    # Put each result in the array of table instances
    foreach my $row (@$array_ref) {
        my $pattern=$$row[0];
        if(locate($pattern, @ids)==0) {
            my $id = shift(@$row);
            push(@ids,$id);
            $metahash{$id}=join("$;",@$row);
        }
    }
    return %metahash;
}

# Get an array for a single column query
sub get_array {
    local($sqls) = @_;
    local(@rows);

    # The statement handle
    # print "$sql\n";
    foreach $sql (split(/;/,$sqls)) {
        my $sth = $dbh->prepare("$sql");

        # Execute the statement
        $sth->execute();
        my $array_ref = $sth->fetchall_arrayref();

        # Put each different row entry in an array
        foreach my $row (@$array_ref) {
            my $pattern=$$row[0];
            if(locate($pattern, @rows)==0) {
                push(@rows,$$row[0]);
            }
        }
    }

    return @rows;
}

# Get a hash for a multi column query
sub get_hash {
    local($table,$sql) = @_;
    local(%rowinfo);
    local(@keys)=split(/$;/,$Primary{$table});

    # The statement handle
    # print "get_hash: $sql<BR>\n";
    my $sth = $dbh->prepare("$sql");

    # Execute the statement
    $sth->execute();
    my $array_ref = $sth->fetchall_arrayref();

    # Put each result in the array of table instances
    foreach my $row (@$array_ref) {

        # Either one or two primary keys, they have to be first  
        my $id=$$row[0];
        if($#keys==2) {
            $id=sprintf("%s:%s",$id,$$row[1]);
        }
        # Add data to hash
        $rowinfo{$id}=join("$;",@$row);
        
    }
    return %rowinfo;

}

# Get all column names in a table - returns an array
sub get_desc {
    local($tablename) = @_;
    local(@rows);

    # The statement handle
    my $prepare_string=sprintf("DESC %s",$tablename);
    my $sth = $dbh->prepare("$prepare_string");

    # Execute the statement
    $sth->execute();
    my $array_ref = $sth->fetchall_arrayref();

    # Put each different row entry in an array
    foreach my $row (@$array_ref) {
        my $pattern=$$row[0];
        if(locate($pattern, @rows)==0) {
            push(@rows,$$row[0]);
        }
    }
    return @rows;
}

# Get all column names in a table - returns an array
sub get_primary {
    local($tablename) = @_;
    local(@rows);

    # The statement handle
    my $prepare_string=sprintf("DESC %s",$tablename);
    my $sth = $dbh->prepare("$prepare_string");

    # Execute the statement
    $sth->execute();
    my $array_ref = $sth->fetchall_arrayref();

    # Put each different row entry in an array
    foreach my $row (@$array_ref) {
        my $pattern=$$row[0];
        if(locate($pattern, @rows)==0) {
            if($$row[3] eq "PRI") {
                push(@rows,$$row[0]);
            }
        }
    }
    return @rows;
}

# Full update keeping untouched columns not named in the METATABLE
sub update_table {
    local($COND,$UPDATE,$myquery)=@_;
    local($TABLE)=$myquery->param("TABLE");
    local($tag)=$myquery->param("TAG");
    
# lock table
    my $dostring="lock tables $TABLE write";
    $dbh->do("$dostring");
# create tmp file with latest entry
    my $prepare_string=sprintf(
         "create temporary table tmp select * from $TABLE where $COND and Version_tag='%s'",$tag);
    my $sth = $dbh->prepare("$prepare_string");
    $sth->execute;
# getting current Modification_date (workarround to the perl DBI bug about wrong nrows from update)
    my $prepare_string=sprintf("select UNIX_TIMESTAMP(Modification_date) from $TABLE where $COND and Version_tag='%s'",$tag);
    my $sth = $dbh->prepare("$prepare_string");
    $sth->execute();
    my $array_row = $sth->fetchall_arrayref();
    my $data_row = $$array_row[0];
    my $modif_date1 = $$data_row[0];
# update entry
    my $dostring=sprintf(
         "update $TABLE set $UPDATE where $COND and Version_tag='%s'",$tag);
    my $nr_rows = $dbh->do("$dostring");
# getting new Modification_date
    my $prepare_string=sprintf("select UNIX_TIMESTAMP(Modification_date) from $TABLE where $COND and Version_tag='%s'",$tag);
    my $sth = $dbh->prepare("$prepare_string");
    $sth->execute();
    my $array_row = $sth->fetchall_arrayref();
    my $data_row = $$array_row[0];
    my $modif_date2 = $$data_row[0];
# comparing both modif dates
    if($modif_date1 != $modif_date2) {
#        $dostring="update tmp set Version_tag=Modification_date,Modification_date=NULL";
        $dostring="update tmp set Version_tag=DATE_FORMAT(Modification_date,GET_FORMAT(TIMESTAMP,'INTERNAL')),Modification_date=NULL";
        $dbh->do("$dostring");
        $dostring="insert into $TABLE select * from tmp";
        $dbh->do("$dostring");
    }
    $dostring="drop table tmp";
    $dbh->do("$dostring");
    $dostring="unlock tables";
    $dbh->do("$dostring");
}

# Delete table from active versions keeping only a copy
sub delete_table {
    local($COND,$myquery)=@_;
    local($TABLE)=$myquery->param("TABLE");
    local($tag)=$myquery->param("TAG");
    
    my $dostring="lock tables $TABLE write";
    $dbh->do("$dostring");
    $dostring=sprintf(
         "update $TABLE set Version_tag=DATE_FORMAT(Modification_date,GET_FORMAT(TIMESTAMP,'INTERNAL')),Modification_date=NULL where $COND and Version_tag='%s'",$tag);
#         "update $TABLE set Version_tag=Modification_date,Modification_date=NULL where $COND and Version_tag='%s'",$tag);
    my $nr_rows = $dbh->do("$dostring");
    $dostring="unlock tables";
    $dbh->do("$dostring");
}

# Store new row in a table
sub store_table {
    local($COLUMNS,$VALUES,$myquery)=@_;
    local($TABLE)=$myquery->param("TABLE");
    
    my $dostring="lock tables $TABLE write";
    $dbh->do("$dostring");
    my $prepare_string="insert into $TABLE ($COLUMNS) values ($VALUES)";
    # print "New: $prepare_string\n";
    my $sth = $dbh->prepare("$prepare_string");
    $sth->execute;
    $dostring="unlock tables";
    $dbh->do("$dostring");
}


# Check if a variable is a number
sub is_number {
    local($numstr)=@_;

    $num=$numstr+0;
    return ("$num" eq "$numstr");
}

# Search an array for an element
sub locate {
    local($patt,@arr)=@_;
    my $is_there = 0;
    foreach $elm (@arr) {
        if ($elm eq $patt) {
            $is_there = 1;
            last;
        }
    }
    return $is_there;
}

#my $test=oct("0x12");
#if(&is_number("$test")) { 
#    print "$test is a number\n"; 
#} else { 
#    print "$test is not a number\n"; 
#}

#my %OrderHash=&get_meta("METATABLE","FRONTEND","colname","orderby");
#my @ordervalue = sort { $OrderHash{$b} <=> $OrderHash{$a} } keys %OrderHash;
#print join(" - ",@ordervalue),"\n";
1; # for require
