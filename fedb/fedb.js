function copyAll(src)
{
  var cell = src.parentNode;
  var cell_col = cell.cellIndex;
  var rows = document.getElementsByTagName('table')[0].rows;
  
  for (var i = 1; i + 1 < rows.length; i++) {
    var row = rows[i];
    var col0 = row.cells[0].children[0];
    var col0value = col0[col0.selectedIndex].value;
    
    if (col0value != 'IGN') {
      var data = row.cells[cell_col].children[0];
      data.value = src.value;
    }
  }
  
}

