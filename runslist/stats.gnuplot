# plot: nruns, nchunks, data size vs. date

set terminal png medium size 850,200
set output outfile
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b %d"
#set xlabel "date"
set ylabel "N/day"
set grid xtics ytics
set key left top

plot infile using 1:($4 / 1024 / 1024 / 1024) with linespoints pt 4 ps 0.2 title "size_Gb", \
     infile using 1:2 with linespoints pt 4 ps 0.2 title "nruns", \
     infile using 1:3 with linespoints pt 4 ps 0.2 title "nchunks"
