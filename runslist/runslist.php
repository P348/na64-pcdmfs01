<html>

<head>
  <title>Runs list</title>
  
  <style type="text/css">
table { border-collapse: collapse; }
thead { position: sticky; top: 0; background: white; }
tr.newrun td { border-top: 2px solid black; }
td { border-right: 1px dotted gray; }
th, td { padding: 2px; }
table td:nth-child(n+6) { text-align: right; }
table td:nth-child(+3) { text-align: right; }
span.today { color: gray; }
table td { font-family: monospace; }
td.empty { border: 0px; }
td.incomplete { background-color: #ede207; }
a.selected { background: #0000DD; color: #FFFFFF; margin: 5px; padding: 5px; text-decoration:none; }
tr:target { background-color: #ffa; }
  </style>
</head>

<body>

<h1>Runs list</h1>

<?php
  // run SQL $query and save result to file $fname
  function dump_query($query, $fname) {
    $query = mysql_query($query);       // do SQL query
    $ncols = mysql_num_fields($query);  // get number of fields in result
    
    // prepare list of fields names
    $names = array();
    for ($i = 0; $i < $ncols; $i++)
      $names[] = mysql_field_name($query, $i);
    
    // prepare header from fields names
    $dump = "#" . implode(" \t", $names) . "\n";
    
    // get content of query
    while ($row = mysql_fetch_row($query))
      $dump .= implode("\t", $row) . "\n";
    
    // save result
    $res = file_put_contents($fname, $dump);
  }
  
  // helper function to sanitize input parameters
  function array_get_item($key, $array, $default, $options) {
    // return default if parameter is not specified
    if (!array_key_exists($key, $array)) return $default;
    
    // return default if parameter value is not one of possible options
    $value = $array[$key];
    if (!in_array($value, $options)) return $default;
    
    return $value;
  }
  
  // display runs list
  function display_runs() {
    // sanitize input period parameter
    $opts = array("10runs", "1day", "1week", "1month", "runs2023B", "runs2023A", "runs2022B", "runs2022A", "runs2021B", "runs2021A", "runs2018", "runs2017", "runs2016B", "runs2016A", "runs2015", "all");
    $period = array_get_item("period", $_GET, "10runs", $opts);
    
    $sizeunits = array_get_item("sizeunits", $_GET, "megabytes", array("megabytes", "bytes"));
    $is_bytes = ($sizeunits == "bytes");
    
    // print period filter menu
    echo "<p>Period: \n";
    foreach ($opts as $i) {
      $class = ($i == $period) ? "selected" : "";
      echo "<a class=\"" . $class . "\" href=\"?period=" . $i . "\">" . $i . "</a>\n";
    }
    echo "</p>\n";
    
    // prepare filter string according to 'period'
    $filter = "TRUE";
    switch ($period) {
      case "10runs" :
        $runs10 = array();
        $query = mysql_query("SELECT DISTINCT RunNumber from CDR_data_files ORDER BY 1 DESC LIMIT 10");
        while ($row = mysql_fetch_row($query)) $runs10[] = $row[0];
        $filter = "RunNumber in (" . implode(",", $runs10) . ")";
        break;
      
      case "1day" : $filter = "Started > '" . date("Y-m-d", strtotime("-1 day")) . "'"; break;
      case "1week" : $filter = "Started > '" . date("Y-m-d", strtotime("-1 week")) . "'"; break;
      case "1month" : $filter = "Started > '" . date("Y-m-d", strtotime("-1 month")) . "'"; break;
      case "runs2015" : $filter = "RunNumber BETWEEN 330 AND 629"; break;
      case "runs2016A" : $filter = "RunNumber BETWEEN 938 AND 1600"; break;
      case "runs2016B" : $filter = "RunNumber BETWEEN 1776 AND 2551"; break;
      case "runs2017" : $filter = "RunNumber BETWEEN 2817 AND 3573"; break;
      case "runs2018" : $filter = "RunNumber BETWEEN 3574 AND 4310"; break;
      case "runs2021A" : $filter = "RunNumber BETWEEN 4642 AND 5187"; break;
      case "runs2021B" : $filter = "RunNumber BETWEEN 5188 AND 5572"; break;
      case "runs2022A" : $filter = "RunNumber BETWEEN 5575 AND 6034"; break;
      case "runs2022B" : $filter = "RunNumber BETWEEN 6035 AND 8458"; break;
      case "runs2023A" : $filter = "RunNumber BETWEEN 8459 AND 9717"; break;
      case "runs2023B" : $filter = "RunNumber BETWEEN 9718 AND 10315"; break;
    }
    
    // DAQ run files min run
    $daqmin = 9780;
    
    // print totals
    $query = mysql_query("SELECT COUNT(*) AS nchunks, COUNT(DISTINCT RunNumber) AS nruns, SUM(Size) AS totsize FROM CDR_data_files WHERE $filter");
    $row = mysql_fetch_assoc($query);
    
    echo "<p>Total: " . $row["nruns"] . " runs, " . $row["nchunks"] . " chunks, " . round($row["totsize"]/1024/1024/1024, 1) . " gigabytes.</p>\n";
    
    // print runs stats plot
    // (plot does not have a sense for < 1 day period or no data)
    $showStatsPlot = !($period == "10runs" || $period == "1day" || $row["nruns"] == 0);
    
    if ($showStatsPlot) {
      $infile = "cache/stats-$period.txt";
      $outfile = "cache/stats-$period.png";
      
      $query = "SELECT
                  DATE_FORMAT(Started, '%Y-%m-%d') AS date,
                  COUNT(DISTINCT RunNumber) AS nrun,
                  COUNT(DISTINCT RunNumber, ChunkNumber) AS nchunk,
                  SUM(Size) AS totalsize,
                  SUM(IsCompleted=0) AS nchunkincomplete
                FROM CDR_data_files
                WHERE $filter
                GROUP BY 1";
      dump_query($query, $infile);
      
      exec("gnuplot -e \"outfile='$outfile'; infile='$infile'\" stats.gnuplot");
      
      echo "<p>\n";
      echo "<a href=\"$infile\"><img src=\"$outfile\"></a><br>\n";
      echo "</p>\n";
    }
    
    // print runs list
    $query = mysql_query("SELECT RunNumber, ChunkNumber, Started, IsCompleted, FileName, Size, CheckSum FROM CDR_data_files WHERE $filter ORDER BY RunNumber DESC, ChunkNumber DESC");
    
    echo "<table id=\"runslist\">\n";
    echo "<thead>\n";
    echo "<tr>\n";
    echo "  <th>Run</th>\n";
    echo "  <th>Chunk</th>\n";
    echo "  <th>Started</th>\n";
    echo "  <th>Status</th>\n";
    echo "  <th>FileName</th>\n";
    echo "  <th>Size</th>\n";
    echo "  <th>CheckSum</th>\n";
    echo "</tr>\n";
    echo "</thead>\n";
    
    $lastrun = "";
    $is_first = true;
    $today_date = date("Y-m-d");
    
    // print table contents
    while ($row = mysql_fetch_assoc($query)) {
      $runn  = $row["RunNumber"];
      $chunk = $row["ChunkNumber"];
      $start = $row["Started"];
      $is_completed = ($row["IsCompleted"] == "1");
      $fname   = $row["FileName"];
      $size = $row["Size"];
      $sum = $row["CheckSum"];
      
      // run
      $run = $runn;
      if ($run != $lastrun) $lastrun = $run; else $run = "";
      $runclass = ($run == "") ? " class=\"empty\"" : "";
      $runid = ($run == "") ? "" : " id=\"$run\" class=\"newrun\"";
      
      // started
      $start_stamp = strtotime($start);
      $start_date = date("Y-m-d", $start_stamp);
      $start_time = date("H:i:s", $start_stamp);
      if ($start_date == $today_date) $start = "<span class=\"today\">" . $start_date . "</span> " . $start_time;
      
      // status
      $status = $is_completed ? "<img src=\"yes.png\">" : "<img src=\"question.gif\">";
      $statusclass = $is_completed ? "" : " class=\"incomplete\"";
      $loglink = "<a href=\"messagelog.php?run=$run\">log</a>";
      $statlink = "<a href=\"http://na64-dev.cern.ch:8080/index.html#/run/http:%2F%2Fna64-dev.cern.ch:8080/run/$run\">stat</a>";
      $infolink = ($run == "") ? "" : " " . $loglink . " | " . $statlink;
      
      // fname
      $fname = "/data/cdr/" . substr_replace($fname, "0", 3, 0);
      // add download link to all finished runs
      // hide link if the newest run chunk has incomplete status to avoid interference with DAQ
      $hidelink = ($is_first && !$is_completed) || ($runn < $daqmin);
      if (!$hidelink) $fname = "$fname <a href=\"$fname\">download</a>";
      
      // size, do not show size of incomplete run chunks
      if (!$is_bytes) $size = round($size / 1024 / 1024) . " <i>Mb</i>";
      
      echo "<tr" . $runid . ">\n";
      echo "  <td" . $runclass . ">$run</td>\n";
      echo "  <td>$chunk</td>\n";
      echo "  <td>$start</td>\n";
      echo "  <td>$status" . $infolink . "</td>\n";
      echo "  <td>$fname</td>\n";
      echo "  <td" . $statusclass . ">$size</td>\n";
      echo "  <td" . $statusclass . ">$sum</td>\n";
      echo "</tr>\n";
      $is_first = false;
    }
    
    echo "</table>\n";
  }
  
  // explicit defaul timezone to avoid warnings
  date_default_timezone_set("Europe/Zurich");
  
  // open database
  if (!mysql_connect("localhost", "daq", "daq")) exit;
  mysql_select_db("DM_DAQ_CONF");
  display_runs();
  mysql_close();
?>

</body>

</html>
