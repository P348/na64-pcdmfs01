# plot: nruns, nchunks, data size vs. date

set terminal png medium size 1000,400
set output outfile
set xlabel "run"
set ylabel "size"
set grid xtics ytics
set key left top

# columns names string
cnames = system(sprintf("head -n1 %s", infile))

# TODO: automate variable number of columns, currently set to up to 15 columns

plot for [i=2:15] infile using 1:i with linespoints pt 4 ps 0.2 title "srcid=".word(cnames,i)
