# plot: number of events per spill vs. run number

set terminal png medium size 1000,300
set output outfile
set xlabel "run"
set ylabel "events per spill"
#set logscale y
set grid xtics ytics
set key left top

plot infile using 1:4 with linespoints pt 4 ps 0.2 title "events/spill"
