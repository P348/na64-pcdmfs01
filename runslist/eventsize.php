<html>

<head>
  <title>Event size</title>
  
  <style type="text/css">
table { border-collapse: collapse; }
thead { position: sticky; top: 0; background: #ddd; }
td { vertical-align: top; border-right: 1px dotted gray; }
th, td { padding: 2px; }
table td:nth-child(-n+3) { text-align: right; }
table td:nth-child(+3) { border-right: 1px solid gray; }
span.today { color: gray; }
table td { font-family: monospace; }
td.empty { border: 0px; }
td.warning { background-color: #ede207; }
a.selected { background: #0000DD; color: #FFFFFF; margin: 5px; padding: 5px; text-decoration:none; }
tr:target { background-color: #ffa; }
tr:hover { background-color: #eee; }
div.plot { border-radius: 5px; padding: 3px; margin-bottom: 2px; display: inline-block; }
div.plot:hover { background-color: #eee; }
  </style>
</head>

<body>

<h1>Event size</h1>

<?php
  // run SQL $query and save result to file $fname
  function dump_query($query, $fname) {
    $query = mysql_query($query);       // do SQL query
    $ncols = mysql_num_fields($query);  // get number of fields in result
    
    // prepare list of fields names
    $names = array();
    for ($i = 0; $i < $ncols; $i++)
      $names[] = mysql_field_name($query, $i);
    
    // prepare header from fields names
    $dump = "#" . implode(" \t", $names) . "\n";
    
    // get content of query
    while ($row = mysql_fetch_row($query))
      $dump .= implode("\t", $row) . "\n";
    
    // save result
    $res = file_put_contents($fname, $dump);
  }
  
  // helper function to sanitize input parameters
  function array_get_item($key, $array, $default, $options) {
    // return default if parameter is not specified
    if (!array_key_exists($key, $array)) return $default;
    
    // return default if parameter value is not one of possible options
    $value = $array[$key];
    if (!in_array($value, $options)) return $default;
    
    return $value;
  }
  
  // display runs list
  function display_runs() {
    // sanitize input period parameter
    $opts = array("10runs", "100runs", "500runs", "runs2023B", "runs2023A", "runs2022B", "runs2022A", "runs2021B", "runs2021A", "all");
    $period = array_get_item("period", $_GET, "10runs", $opts);
    
    // print period filter menu
    echo "<p>Period: \n";
    foreach ($opts as $i) {
      $class = ($i == $period) ? "selected" : "";
      echo "<a class=\"" . $class . "\" href=\"?period=" . $i . "\">" . $i . "</a>\n";
    }
    echo "</p>\n";
    
    $query = mysql_query("SELECT MAX(run_number) from Monitoring_srcid WHERE run_number < 900000");
    $row = mysql_fetch_row($query);
    $maxrun = $row[0];
    //echo "<p>maxrun = " . $maxrun . "</p>\n";
    
    // prepare filter string according to 'period'
    $filter = "TRUE";
    switch ($period) {
      case "10runs" : $filter = "run_number BETWEEN " . ($maxrun-10+1)  . " AND " . $maxrun; break;
      case "100runs" : $filter = "run_number BETWEEN " . ($maxrun-100+1)  . " AND " . $maxrun; break;
      case "500runs" : $filter = "run_number BETWEEN " . ($maxrun-500+1)  . " AND " . $maxrun; break;
      case "runs2015" : $filter = "run_number BETWEEN 330 AND 629"; break;
      case "runs2016A" : $filter = "run_number BETWEEN 938 AND 1600"; break;
      case "runs2016B" : $filter = "run_number BETWEEN 1776 AND 2551"; break;
      case "runs2017" : $filter = "run_number BETWEEN 2817 AND 3573"; break;
      case "runs2018" : $filter = "run_number BETWEEN 3574 AND 4310"; break;
      case "runs2021A" : $filter = "run_number BETWEEN 4642 AND 5187"; break;
      case "runs2021B" : $filter = "run_number BETWEEN 5188 AND 5572"; break;
      case "runs2022A" : $filter = "run_number BETWEEN 5575 AND 6034"; break;
      case "runs2022B" : $filter = "run_number BETWEEN 6035 AND 8458"; break;
      case "runs2023A" : $filter = "run_number BETWEEN 8459 AND 9717"; break;
      case "runs2023B" : $filter = "run_number BETWEEN 9718 AND 10315"; break;
    }
    
    // query data
    $query = "SELECT
                run_number,
                source_id,
                COUNT(spill_number) AS total_spill_number,
                SUM(event_count) AS total_event_count,
                SUM(data_size) AS total_data_size
              FROM Monitoring_srcid
              WHERE $filter
              GROUP BY run_number DESC, source_id ASC";
    $query = mysql_query($query);
    
    $d = array();
    $d2 = array();
    $d3 = array();
    $d3e = array();
    $d4 = array();
    
    while ($row = mysql_fetch_row($query)) {
      $run  = $row[0];
      $srcid = $row[1];
      $nspills = $row[2];
      $nevts = $row[3];
      $nsize   = $row[4];
      
      $d[$run][$srcid] = (int) ($nsize / $nevts);
      $d2[$run][$srcid] = $row;
      
      $nspills0 = array_key_exists($run, $d3) ? $d3[$run] : 0;
      $d3[$run] = max($nspills, $nspills0);
      
      $nevts0 = array_key_exists($run, $d3e) ? $d3e[$run] : 0;
      $d3e[$run] = max($nevts, $nevts0);
      
      if (!array_key_exists($run, $d4)) $d4[$run] = array("spills" => 0, "events" => 0);
      $d4[$run]["spills"] += $nspills;
      $d4[$run]["events"] += $nevts;
    }
    
    //echo "<pre>\n";
    //print_r($d3);
    //echo "</pre>\n";
    
    // print totals
    echo "<p>Total: " . count($d) . " runs, " . array_sum($d3) . " spills, " . array_sum($d3e) . " events.</p>\n";
    
    
    // dump per-srcid average event size
    // prepare list of fields names
    $srcid_list = array();
    foreach ($d as $i => $v)
      foreach ($v as $id => $avgsize)
        $srcid_list[$id] = 1;
    
    //$names = array("run");
    $names = array_keys($srcid_list);
    sort($names);
    
    //echo "<pre>\n";
    //print_r($names);
    //echo "</pre>\n";
    
    // prepare header from fields names
    $dump = "#run \t" . implode(" \t", $names) . "\n";
    
    // get content of query
    foreach ($d as $i => $v) {
      $dump .= "$i \t";
      foreach ($names as $id) {
        $avgsize = array_key_exists($id, $v) ? $v[$id] : 0;
        $dump .= "$avgsize \t";
	  }
      $dump .= "\n";
	}
    
    // save result
    $fname = "cache/eventsize-$period-avgsize.txt";
    $res = file_put_contents($fname, $dump);

    $outfile = "cache/eventsize-$period-avgsize.png";
    exec("gnuplot -e \"outfile='$outfile'; infile='$fname'\" eventsize.gnuplot");
    
    echo "<div class=\"plot\">\n";
    echo "Average size of single event (srcid= 2-5 MSADC, 621 GEM, 622 MM, 760-762 STRAW, 945 MUX {sum over all detectors}):<br>\n";
    echo "<a href=\"$fname\"><img src=\"$outfile\"></a><br>\n";
    echo "</div>\n";
    echo "<br>\n";
    
    
    // => events_per_spill data file
    // header
    $dump = sprintf("#%9s %10s %10s      %s\n", "run", "nspills", "nevents", "events_per_spill");
    
    // data table
    foreach ($d4 as $i => $v) {
      $events_per_spill = (int) ($v["events"] / $v["spills"]);
      $dump .= sprintf("%10d %10d %10d %10d\n", $i, $d3[$i], $d3e[$i], $events_per_spill);
    }
    
    // save result
    $fname = "cache/eventsize-$period-run_spills_events.txt";
    file_put_contents($fname, $dump);
    
    $outfile = str_replace(".txt", ".png", $fname);
    exec("gnuplot -e \"outfile='$outfile'; infile='$fname'\" eventsize_eps.gnuplot");
    
    echo "<div class=\"plot\">\n";
    echo "Average number of events per spill:<br>\n";
    echo "<a href=\"$fname\"><img src=\"$outfile\"></a><br>\n";
    echo "</div>\n";
    echo "<br>\n";
    
    // print table contents
    echo "<table id=\"runslist1\">\n";
    echo "<thead>\n";
    echo "<tr>\n";
    echo "  <th>Run</th>\n";
    echo "  <th>nspills</th>\n";
    echo "  <th>nevents</th>\n";
    foreach ($names as $i)
      echo "  <th>srcid$i</th>\n";
    echo "</tr>\n";
    echo "</thead>\n";
    
    foreach ($d2 as $nrun => $v) {
      $nspillstot = $d3[$nrun];
      $neventstot = $d3e[$nrun];
      echo "<tr>\n";
      echo "  <td>$nrun</td>\n";
      echo "  <td>$nspillstot</td>\n";
      echo "  <td>$neventstot</td>\n";
      
      foreach ($names as $id) { // loop over srcid
      $isAvailable = array_key_exists($id, $v);
      $status = array();
      $isGood = true;
      
      if ($isAvailable) {
        $row = $v[$id];
        //$run  = $row[0];
        //$srcid = $row[1];
        $nspills = $row[2];
        $nevents = $row[3];
        $nsize   = $row[4];
        $avgsize1 = (int) ($nsize / $nevents);
      
        $status[] = "size1=$avgsize1";
        if ($nspills != $nspillstot) {
          $status[] =  "nspills=$nspills";
          $isGood = false;
        }
        if ($nevents != $neventstot) {
          $status[] =  "nevents=$nevents";
          $isGood = false;
        }
      }
      else {
        $status[] = "N/A";
        $isGood = false;
      }
      
      $good = $isGood ? "" : "warning";
      echo "  <td class=\"$good\">";
      echo implode("<br>", $status);
      }
      echo "</tr>\n";
    }
    
    echo "</table>\n";
    
    //echo "<pre>\n";
    //print_r($d);
    //echo "</pre>\n";
  }
  
  // explicit defaul timezone to avoid warnings
  date_default_timezone_set("Europe/Zurich");
  
  // open database
  if (!mysql_connect("localhost", "daq", "daq")) exit;
  mysql_select_db("DM_DAQ_CONF");
  display_runs();
  mysql_close();
?>

</body>

</html>
