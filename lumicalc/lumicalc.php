<html>

<head>
  <title>Lumicalc</title>
  <style type="text/css">
h2 { color: gray; }
table { border-collapse: collapse; }
th, td { padding: 5px; }
tr:hover { background-color: #DDD; }
table td:nth-child(n+2) { text-align: right; }

div#trendplot {
  width: 90%;
  height: 600;
  border: 1px dashed black;
  /* margin: 5px 5px 5px 50px; */
  /* padding: 10px 10px 10px 10px; */
}

.dygraph-legend {
  left: 100px !important;
  width: 15em !important;
  opacity: 0.9;
  padding: 10px 10px 10px 10px;
}

.dygraph-legend > span.highlight {
  font-weight: bold;
}
  </style>
  
  <!-- http://dygraphs.com/ -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/dygraph/2.1.0/dygraph.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dygraph/2.1.0/dygraph.min.css" />
</head>

<body>

<h1>Scalers</h1>

<?php
  // explicit defaul timezone to avoid warnings
  date_default_timezone_set("Europe/Zurich");
  
  $scalers = "/online/detector/scalers/scalers2018.log";
  
  // input data file
  $f = fopen($scalers, "rb");
  
  // totals
  $tot = array_fill(0, 9, 0);
  $rec = array_fill(0, 9, 0);
  $ntot = 0;
  $nrec = 0;
  $first = array();
  $last = array();
  
  function asum($a, $b) { return $a+$b; }
  
  // trends data
  $dump = "";
  $dump .= "Timestamp,EOT Total,EOT Recorded,EOT Lost,EOT Instant\n";
  
  while (($i = fgets($f)) !== false) {
    $i = trim($i);
    $i = preg_replace("/\s+/", " ", $i);
    $x = explode(" ", $i);
    $n = count($x);
    
    // check format
    $isOK = (($n == 17) && ($x[1] == "Run") && ($x[3] == "Spill"));
    if (!$isOK) continue;
    
    // NA64 DIM service for scalers: S1 S2 S1-S4_Vbar S1-S4_VbarBUSY TRIGGER TCS_TRIGGER Thu May 24 15:02:44 2018
    // 1527462017 Run   4047 Spill   62  6509256 399046587 5954233 365030347 5184094 317898189 4347129 266938292 6003 365583 6084 370605
    
    // extract timestamp, run, spill, counters*6
    $r      = array((int)$x[0], (int)$x[2], (int)$x[4],
                    (int)$x[5], (int)$x[7], (int)$x[9], (int)$x[11], (int)$x[13], (int)$x[15]);
    
    $tot = array_map("asum", $tot, $r);
    $ntot++;
    
    $isRecorded = ($r[1] < 9999) && ($r[2] > 0);
    if ($isRecorded) {
      if (count($first) == 0) $first = $r;
      $last = $r;
      
      $rec = array_map("asum", $rec, $r);
      $nrec++;
    }
    
    $dump .= date("Y-m-d H:i:s", $r[0]) . "," . $tot[6]  . "," . $rec[6] . "," . ($tot[6] - $rec[6]) . "," . $r[6] . "\n";
  }
  
  fclose($f);
  
  file_put_contents("cache/scalers.txt", $dump);
  
  echo "<h2>Data source</h2>\n";
  echo "<p>\n";
  // TODO: download link is hardcoded now
  echo "<b>Path</b>: " . $scalers . " (<a href=\"" . "scalers2018.log" . "\">download</a>)<br>\n";
  $lastUpdate = filemtime($scalers);
  echo "<b>Last update</b>: " . date("r", $lastUpdate) . " (" . (time() - $lastUpdate) .  " seconds ago)<br>\n";
  printf("<b>Runs range</b>: run %d spill %d - run %d spill %d<br>\n", $first[1], $first[2], $last[1], $last[2]);
  printf("<b>Time range</b>: %s - %s<br>\n", date("r", $first[0]), date("r", $last[0]));
  
  echo "<h2>Summary</h2>\n";
  
  $cname = array("S1", "S2", "S1-S4_Vbar", "S1-S4_VbarBUSY", "TRIGGER", "TCS_TRIGGER");
  
  // table data
  $table = array_combine($cname, array_map(null, $cname, array_slice($rec, 3), array_slice($tot, 3)));
  
  echo "<table>\n";
  echo "<tr><th>Counter</th><th>Instant (average per spill)</th><th>Recorded</th><th>Total</th></tr>\n";
  foreach ($table as $c => $r) {
    echo "<tr>";
    echo "<td>$r[0]</td>";
    printf("<td>%d</td>", $r[1]/$nrec);
    printf("<td>%.2e</td>", $r[1]);
    printf("<td>%.2e</td>", $r[2]);
    echo "</tr>\n";
  }
  echo "</table>\n";
  
  echo "<p>NOTE: 'Total' counts are underestimate of the actual delivered amount.</p>\n";
  
  echo "<h2>Efficiencies</h2>\n";
  echo "<p>\n";
  $daqeff = 100 * $table["S1-S4_VbarBUSY"][1] / $table["S1-S4_Vbar"][1];
  $dataeff = 100 * $table["S1-S4_VbarBUSY"][1] / $table["S1-S4_VbarBUSY"][2];
  printf("<b>Beam telescope</b> = S1-S4_Vbar / S1 = %.1f%%<br>", 100 * $table["S1-S4_Vbar"][1] / $table["S1"][1]);
  printf("<b>DAQ</b> = S1-S4_VbarBUSY / S1-S4_Vbar = %.1f%% (dead time = %.1f%%)<br>", $daqeff, 100 - $daqeff);
  printf("<b>Full</b> = S1-S4_VbarBUSY / S1 = %.1f%%<br>", 100 * $table["S1-S4_VbarBUSY"][1] / $table["S1"][1]);
  printf("<b>Trigger reduction ratio</b> = S1-S4_VbarBUSY / TRIGGER = %d<br>", $table["S1-S4_VbarBUSY"][1] / $table["TRIGGER"][1]);
  printf("<b>Data recording</b> = S1-S4_VbarBUSY<sub>recorded</sub> / S1-S4_VbarBUSY<sub>total</sub> = %.1f%% (dry run = %.1f%%)<br>", $dataeff, 100 - $dataeff);
  
?>


<h2>Trend plots</h2>

<p>Electrons On Target (EOT) is S1-S4_VbarBUSY counter.</p>

<div id="trendplot" class="many"></div>
<script type="text/javascript">
  var g = new Dygraph(
    document.getElementById("trendplot"),
    "cache/scalers.txt", {
      legend: 'always',
      labelsSeparateLines: true,
      rightGap: 20,
      highlightSeriesOpts: {
        strokeWidth: 4,
        highlightCircleSize: 5
      },
      strokeWidth: 2,
      axes: {
        y: {
          axisLabelWidth: 70
        }
      }
    }
  );
</script>

<?php
  //echo "<p>Memory=" . memory_get_usage(false)/1e6 . " Mb</p>";
?>

</body>

</html>
