<html>

<head>
  <title>Databases and tables</title>
  <style type="text/css">
.dbname { font-weight: bold; }
a.tablename { background: #DDD; margin: 0px; padding: 1px; text-decoration:none; }
a.selected { background: #0000DD; color: #FFFFFF;}
#data td { vertical-align: top; white-space: pre; font-family: monospace; }
tr.unknown { background: #ffc; }
tr.change { background: #cfc; }
tr.error { background: #fcc; }
/*
#data td:nth-child(1):hover { background: #DDD; }
#data thead { position: sticky; top: 0; left: 0; }
*/
</style>
</head>

<body>

<h1>Databases and tables</h1>

<?php
  // function returns the list of databases and tables
  function list_tables() {
    $tables = array();
    
    // list all available databases
    $query = mysql_list_dbs();
    while ($row = mysql_fetch_object($query)) {
      $db = $row->Database;
      $tables[$db] = array();
    }
    
    // list tables in databases
    foreach (array_keys($tables) as $db) {
      mysql_select_db($db);
      $query = mysql_query("SHOW TABLES");
      while ($row = mysql_fetch_row($query)) {
        $tables[$db][] = $row[0];
      }
    }
    
    return $tables;
  }
  
  // display contents of the table
  function display_table($table, $maxn) {
    $filter = "";
    
    // reverse sort for runslist table
    if ($table == "CDR_data_files") $filter = "ORDER BY ChunkId DESC";
    
    // reverse sort for Message_log
    if ($table == "Message_log") $filter = "ORDER BY Id DESC";
    if ($table == "Message_log") $maxn = 500;
    
    // reverse sort for Monitoring_srcid
    if ($table == "Monitoring_srcid") $filter = "ORDER BY id DESC";
    
    $query = mysql_query("SELECT COUNT(*) FROM $table");
    $row = mysql_fetch_row($query);
    echo "<p>Total entries: $row[0]. Display limited to $maxn entries.</p>\n";
    
    echo "<table id=\"data\" border=1>\n";
    
    $query = mysql_query("SELECT * FROM $table $filter LIMIT $maxn");
    
    // print columns names
    echo "<tr>\n";
    $ncols = mysql_num_fields($query);  // get number of fields in result
    for ($i = 0; $i < $ncols; $i++) {
      $name = mysql_field_name($query, $i);
      echo "  <th>$name</th>\n";
    }
    echo "</tr>\n";
    
    // print table contents
    while ($row = mysql_fetch_row($query)) {
      $id = $row[0];
      $warn = "";
      if ($table == "Message_log") {
        // unknown message
        $warn = "unknown";
    
        // normal progress messages
        if ($row[6] == "infM") $warn = "";
        if ($row[6] == "infR") $warn = "";
        if ($row[6] == "infD") $warn = "";
        if ($row[6] == "info") $warn = "";
        
        // run type change messages
        if ($row[6] == "infC") $warn = "change";
        
        // error message
        if ($row[6] == "errm") $warn = "error";
        if ($row[6] == "errR") $warn = "error";
        if ($row[6] == "errC") $warn = "error";
        if ($row[6] == "derr") $warn = "error";
        if ($row[6] == "warR") $warn = "error";
        if ($row[6] == "warC") $warn = "error";
      }
      echo "<tr id=\"$id\" class=\"$warn\">\n";
      $alink = "<a href=\"#$id\">#</a>";
      
      foreach ($row as $field) {
        echo "  <td>$field$alink</td>\n";
        $alink = "";
      }
      echo "</tr>\n";
    }
    
    echo "</table>\n";
  }
  
  // open database
  if (!mysql_connect("localhost", "daq", "daq")) exit;
  
  // print list of databases and tables
  $list = list_tables();
  
  // hide metadata table
  unset($list["information_schema"]);
  
  echo "<ul>\n";
  foreach ($list as $db => $tables) {
    echo "  <li><span class=\"dbname\">" . $db . "</span>:\n";
    
    foreach ($tables as $i) {
      $sel = ("$db.$i" == $_GET["table"]) ? "selected" : "";
      echo "    <a class=\"tablename $sel\" href=\"?table=$db.$i\">$i</a>\n";
    }
    
    echo "</li>\n";
  }
  echo "</ul>\n";
  echo "\n";
  
  // extract query
  list($db, $table) = explode(".", $_GET["table"]);
  $nentries = intval($_GET["nentries"]);
  
  // sanitize input:
  // - check the db/table exists
  if (!array_key_exists($db, $list)) exit;
  if (!in_array($table, $list[$db])) exit;
  // - limix max entries
  if ($nentries == 0) $nentries = 200;
  if ($nentries > 200) $nentries = 200;
  
  // print table contents
  echo "<h3 id=\"table\">Table $db.$table</h3>\n";
  mysql_select_db($db);
  display_table($table, $nentries);
  echo "\n";
  
  mysql_close();
?>

</body>

</html>
