<html>

<head>
  <title>Detectors trends</title>
  
  <!-- auto-refresh web page each 10 minutes, actual plots update cycle is 60 minutes -->
  <meta http-equiv="refresh" content="600">
  
  <style type="text/css">
a.item {background: #fafafa; text-decoration: underline dashed; margin: 5px; padding: 5px; }
a.item:hover { background: #eee; }
div.plot { background: #eee; border-radius: 5px; padding: 3px; margin-bottom: 5px; display: inline-block; width: 400px; }
div.plot:target { background-color: #CFC; }
div.plot img { width:100%; }
p.details { font-style: italic; }
  </style>
</head>

<body>

<h1>Detectors trends</h1>

<?php
    // explicit timezone to output local time
    date_default_timezone_set("Europe/Paris");
  
    // enumerate pictures
    $list = glob("cache/trendview-*.png");
    //echo "<pre>\n";
    //print_r($list);
    //echo "</pre>\n";
    
    // extract presets names
    $opts = array();
    foreach ($list as $i) {
      $vals = sscanf($i, "cache/trendview-%s.png");
      if (!$vals) continue;
      
      $opts[] = str_replace(".png", "", $vals[0]);
    }
    //$opts = array("led", "xy", "scalers", "calo");
    
    // last point metadata: run spill time
    $meta = explode(" ", file_get_contents("cache/trendview.txt"));
    // last plots update time
    $modtime = filemtime("cache/trendview.txt");
    
    echo "<p>\n";
    echo "Last plots update: " . date("D, d M Y H:i:s", $modtime) . " (" . round((time(0)-$modtime)/60) . " min ago)<br>\n";
    echo "Last data point: " . date("D, d M Y H:i:s", $meta[2]) . " (" . round((time(0)-$meta[2])/60) . " min ago, run " . $meta[0] . ")\n";
    echo "</p>\n";
    
    // print menu
    echo "<p>Subsystem: \n";
    foreach ($opts as $i) {
      echo "<a class=\"item\" href=\"#" . $i . "\">" . $i . "</a>\n";
    }
    echo "</p>\n";
    
    echo "<hr>\n";
    echo "<p class=\"details\">Need more details? Use <b>StartReview.sh</b> on any of run-control machines (pcdmrcNN).</p>\n";
    
    // print pictures
    foreach ($opts as $i) {
    $pict = "cache/trendview-$i.png";
    echo "<div id=\"$i\" class=\"plot\">\n";
    echo "$i:<br>\n";
    echo "<a href=\"$pict\"><img src=\"$pict\"></a>\n";
    echo "</div>\n";
    echo "\n";
    }
    
?>

</body>

</html>
